# Setup

* [Install Node](http://nodejs.org/download/) 
* [Install Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md#getting-started)
* Clone the repo
* CD to repo's location in terminal
* Run **npm install**
* Run **gulp watch** command

## Project structure

The project base is split into **two** main directories - **dev** and **dist** which are sat at the root level of the project.

**Dev**(development) - The primary working directory, all build activity will take place here.

**Dist**(distribution) - The location of the completed build, all files here will be in their final form - compressed, minified and optimised ready for deployment. **Files should never be edited here.**

## Development

###HTML

 *  Uses a **stage system** enabling the use of html includes for flexibility e.g header and footer
 * As such all html files should be created within the **dev/stage** folder and any partial files to be used as includes should be created in the **dev/stage/includes** folder
 * No editing should take place on the html files in the 
**dev/** location
 * Includes belong in the page template files(html files created at the **dev/stage** level) and are achieved with the following syntax - **@@include('includes/FILENAME_HERE.html')**

###SCSS/CSS

* All style development takes place in the **dev/scss/** location using a modular approach
* Styling is achieved between the files in the folders **base(all site wide  styling), layout(page level styling), modules(component styling), tools(logic for mixins and placeholders) and vendor(styling for/from any pluggins)**
* Don't edit any scss files in the **dev/scss/bourbon** folder
* Bourbon neat can receive changes to the scss but the only file that would require any adjustment is the **dev/scss/neat/settings/_grid.scss** file which contains various variables that tie in with the grid itself
* **Layout** and **modules** are the areas most likely to have files added, ensure these are included in the **dev/scss/master.scss** file to ensure they get compiled when compilation takes place
* All styling must be done in the **dev/scss** files, the **dev/js/init.js** should never be touched as changed will only be overwritten when the js compiling takes place

### JS

* jQuery 2.1.1 is included by default
* All scripting to be done in the **dev/js/scripts.js** file, within the provided document ready function
* Any third-party pluggins or libraries can be used and must be placed into the **dev/js/vendor** folder, these are then concatenated, along with jQuery and the scripts.js file to create **dev/js/init.js**, this is the file that the html references in the foooter
* All scripting must be done in the **dev/js/scripts.js** file, the **dev/js/init.js** should never be touched as changed will only be overwritten when the js compiling takes place

## Commands

* **Gulp watch**
 * Sets gulp to watch for changes in either the **html**, **scss** or the **js** files
 * will automatically update the browser unless an error is encountered
 * Produces feedback(success or failure compilation reports) via Mac OS X notifications 
* **Gulp build**
 * Compiles the **html**, **scss** or the **js** files, performs the same action in regards to file compilation that **Gulp watch** carries out with the only difference being that this is a manual initiation
* **Gulp publish**
 * Takes the build files located in the **dev/** directory and prepares them for deployment - concatenation and minification of js, minification of css and html, images are also copied over and optimised

## References
* [Modernizr](http://modernizr.com/) - Modernizr is a JavaScript library that detects HTML5 and CSS3 features in the user’s browser
* [Bourbon](http://bourbon.io/docs/) - A mixin library for all your SASS utility goodness
* [Bourbon neat](http://neat.bourbon.io/docs/) - A lightweight semantic grid framework for Sass and Bourbon