
//  Setup base Gulp plugins/dependencies
// ====================================================== //

    var gulp = require('gulp'),
        gulpLoadPlugins = require('gulp-load-plugins'),
        bourbon = require('node-bourbon'),
        neat = require('node-neat'),
        browserSync = require('browser-sync'),
        plugins = gulpLoadPlugins();

// Paths object for quick reference and cleaner code 
// ====================================================== //

    var dir = {
        dev: './dev/',
        dist: './dist/'
    }

    var paths = {

        html: {
            files:  dir.dev + 'stage/**/*.html',
            compiled: dir.dev + '*.html'
        },
        styles: {
            files: dir.dev + 'scss/**/*.scss',
            compiled: dir.dev + 'css/styles.css',
            output: 'styles.css',
            dest: dir.dev + 'css/',
            live: dir.dist + 'css/'
        },
        js: {
            files: dir.dev + 'js/*.js',
            src: dir.dev + 'js/',
            vendor: dir.dev + 'js/vendor/*.js',
            pre: dir.dev + 'js/scripts.js',
            post: 'init.js',
            built: dir.dev + 'js/init.js',
            live: dir.dist + 'js'
        },
        images: {
            files: dir.dev + 'img/*.+(jpg|png|gif)',
            live: dir.dist + 'img/'
        }

    }

// Tasks 
// ====================================================== //

    // Create html files
    gulp.task('html', function () {
        return gulp.src(paths.html.files)
            .pipe(plugins.fileInclude({
                prefix: '@@',
                basepath: '@file'
            }))
            .pipe(gulp.dest(dir.dev))
            .pipe(plugins.notify("👍  html successfully built"))
            .pipe(browserSync.reload(
                {stream:true})
            );
    });

    gulp.task('minify-html', function () {

        var opts = {
            keepSpecialComments: false
        };

        gulp.src(paths.html.compiled)
            .pipe(plugins.minifyHtml(opts))
            .pipe(gulp.dest(dir.dist))
            .pipe(plugins.notify("👍  html successfully minified"));
    });

    // Compile SCSS and prefix css
    gulp.task('css', function () {
        gulp.src([paths.styles.files])
            .pipe(plugins.sass({
                outputStyle: 'expanded',
                includePaths: ['scss'].concat(neat.includePaths),
                errLogToConsole: false,
                onError: function(err) {
                    return plugins.notify().write('❗❗❗❗❗❗❗❗❗❗❗❗❗❗❗❗❗ ' + err);
                }
            }))
            .pipe(plugins.autoprefixer('last 2 versions', 'ie 9'))
            .pipe(plugins.rename(paths.styles.output))
            .pipe(gulp.dest(paths.styles.dest))
            .pipe(plugins.notify("👍  SCSS successfully compiled"))
            .pipe(browserSync.reload(
                {stream:true}
            ));
    });

    // Sort and organise CSS
    gulp.task('comb', function () {
        return gulp.src([dir.dev + 'scss/**', '!' + dir.dev + 'scss/bourbon/**', '!' + dir.dev + 'scss/neat/**' ], {base: './'})
            .pipe(plugins.csscomb())
            .pipe(gulp.dest('./'))
            .pipe(plugins.notify("👍  Styles successfully sorted"));
    });

    // Minify CSS
    gulp.task('minify-css', function () {
        gulp.src(paths.styles.compiled)
            .pipe(plugins.minifyCss())
            .pipe(gulp.dest(paths.styles.live))
            .pipe(plugins.notify("👍  CSS successfully minified"));
    });

    // Concatenate JS
    gulp.task('js', function () {
        gulp.src([paths.js.vendor, paths.js.pre])
            .pipe(plugins.concat(paths.js.post))
            .pipe(gulp.dest(paths.js.src))
            .pipe(plugins.notify("👍  JS successfully compiled"))
            .pipe(browserSync.reload(
                {stream:true}
            ));
    });

    gulp.task('minify-js', function () {
        gulp.src(paths.js.built)
            .pipe(plugins.uglify())
            .pipe(gulp.dest(paths.js.live))
            .pipe(plugins.notify("👍  JS successfully minified"));
    });

    // Image optimisation
    gulp.task('images', function () {
        return gulp.src(paths.images.files)
            .pipe(plugins.imagemin({
                optimizationLevel: 4,
                progressive: true
            }))
        .pipe(gulp.dest(paths.images.live))
        .pipe(plugins.notify("👍  Images optimised"));
    });

    // Browser-sync task for starting the server.
    gulp.task('browser-sync', function() {
        
        var files = [
            paths.html.compiled,
            paths.styles.compiled,
            paths.images.files,
            paths.js.compiled
        ];

        browserSync.init(null, {
            server: {
                baseDir: dir.dev
            }
        });

    });

// Actions 
// ====================================================== //

    gulp.task('watch', ['browser-sync'],  function() {

        // Watch html files
        gulp.watch(paths.html.files, function() {
            gulp.run('html');
        });
          
        // Watch scss files
        gulp.watch(paths.styles.files, function() {
            gulp.run('comb', 'css');
        });

        // Watch js
        gulp.watch(paths.js.files, function() {
            gulp.run('js');
        });

    });

    // Compile site files
    gulp.task('build', ['html', 'css', 'js']);

    // Run to optimise site for delivery */
    gulp.task('publish', ['minify-html', 'minify-css', 'minify-js', 'images']);


